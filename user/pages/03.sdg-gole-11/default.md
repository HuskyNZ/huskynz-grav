---
title: 'Sdg gole 11'
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

Sdg goal 11

What is a SDG goal? A SDG goal is a set of targets that the un wants to meet by 2030. What is the un? The un is a group countries that come Together and discuss polices and or changes for the world.

What is SDG goal 11? SDG goal 11 focuses on sustainable cities and communities gives a set of guide lines for countries to meet the goal by 2030
What Countries are doing it well?
This shows what percent of contres citizens are living in slums

According to sdg-tracker.org “"By 2030, ensure access for all to adequate, safe and affordable housing and basic services and upgrade slums"” meaning that the want to get every one fair housing by 2030. The data to back it up is here all the gray countries they have no data for meaning that they are doing better than the ones in red
![Slum pop word](https://bitbucket.org/HuskyNZ/resources_images/raw/c8830672432f560b10ee140b77de371ddd1c6577/share-of-urban-population-living-in-slums.png)


for example
---------------------------------------------------
Africa Eastern and S	1990	60.30623
------------------------------------------------------
Africa Eastern and S	1995	58.18435	
------------------------------------------------------
Africa Eastern and S	2000	58.58584	
------------------------------------------------------
Africa Eastern and S	2005	61.17015	
--------------------------------------------------------
Africa Eastern and S	2010	55.6518	
--------------------------------------------------------
Africa Eastern and S	2014	58.7544
-------------------------------------------------------
Africa Eastern and S	2016	57.19622	
-------------------------------------------------------
Africa Eastern and S	2018	55.77097
-------------------------------------------------------
This shows the percent of population in Africa Eastern from 1990 to 2018 it went from 60% of the total population down to 55% this means that Africa is becoming more rich and more people are able to afford housing

More data can be found here
What organizations are helping?

City Water Resilience Approach (CWRA)

Water crises – too much, too little and polluted water are already affecting people’s health and wellbeing, devastating economies and threatening lives and livelihoods in many countries around the world. Due to the combined impact of climate change, human action and population growth, a recent UN-…[more] Partners Arup, Stockholm International Water Institute (SIWI), Resilient Cities Network (RCN), World Bank, Rockefeller Foundation, Resilience Shift, University of Massachusetts, OECD, Alliance for Global Water…[more] Action NetworkSDG Acceleration Actions Sustainable Development Goals

CSTI Circular Bioeconomy Consortium

Core focus: Digitalization of Green Infrastructure Created in 1998, the Centre for Science and Technology Innovations (CSTI) is a Kenya registered trust associated with UNESCO and affiliated with the Kenya National Academy of Sciences. Our mission is to improve lives through scien…[more] Partners Lead: Centre for Science and Technology Innovations (CSTI)\r\nLiquid Intelligent Technologies Geviton Enterprise Paraclete Consult Frontline Impact Besic Group Centre Internati…[more] Sustainable Development Goals

The UCLan Research Centre for Applied Sport, Physical Activity and Performance

The objective is to address the SDG’s through Sport and Health Science research, curriculum and pedagogy through a virtual Global Pedagogy for Knowledge Exchange . Partners University of Central Lancashire Action NetworkHigher Education Sustainability Initiative Sustainable Development Goals

The Canvas

The Canvas transforms vacant spaces into hubs focused on sustainable development. We empower global rising, ethical businesses and provide them with access to global markets, services and technologies. We believe the demise of fast fashion will be accelerated through a connected network of sustainab…[more] Partners 3QUARTERS, DOTMO (Distant Relatives), Abby Alley, ADIFF, Amalya Meira, Anne James New York, Arielle, Atelier Minimalist, AYANI, Qaytu, Declare Denim, Tove by Design, Vert Toi, Reuben Oliver Co., Bring…[more] Action NetworkConscious Fashion and Lifestyle Network Sustainable Development Goals

The development of Charter Cities

The development of charter cities will put people at the center, while better managing rapid urbanization and accelerating SDG implementation. In particular, charter cities will: 1. Fosters socio-economic advancement, increase productivity, and improve quality of life. 2. Enhance societal freedo…[more] Partners Center for Global Development Enyimba Economic City Talent City Ciudad Morazan Nkwashi African School of Economics African Center for Study of United States (Check) Perim Associates Thebe Inve…[more] Action NetworkSDG Acceleration Actions Sustainable Development Goals

Green-Gray Community of Practice

The Global Green-Gray Community of Practice, created in June 2020 and led by Conservation International, is an international collaboration across the conservation, engineering, finance, and construction sectors to generate learning and innovation to achieve climate adaptation benefits for communitie…[more] Partners The Green-Gray Community of Practice includes 90+ members across sectors and entity types including private companies, universities, research institutions, non-governmental organizations, municipal en…[more] Action NetworkThe Ocean Conference Sustainable Development Goals

Demonstration Pilot of Water- Energy- Food -Safety- Ecology -Community- Health (WEFSECH)

Integrated seafarming to provide clean Water – Energy – Food – Safety – Ecology – Community – Health systems Partners OceanNexus Action NetworkSDG Acceleration Actions Sustainable Development Goals

WATER RESOURCES

The key incremental actions will be to support: (i) The preparation of a binational TDA and SAP to have an agreed framework of action. The SAP will be an updated version of what is currently called the Binational Masterplan, but incorporating (a) the conceptual basis of IWRM, (b) new considerations …[more] Partners Ministry of Environment and Water (Bolivia), Ministry of Foreign Affaris of the Plurinational State of Bolivia, Ministry of Enviroment (Peru), Ministry of Foreign Affairs of Peru Action NetworkSDG Acceleration Actions Sustainable Development Goals

In conclusion SDG goal, 11 is important if we want to have countries survive with people because if people live in slums they are more likely to catch diseases as they spread more easily than in built-up places. SDG 11 will help the poor and grow communities in countries which may not survive into the next decade.

All the sorces are below
Goal 11: Make cities inclusive, safe, resilient and sustainable Un source
  Sustainable Development Goal 11         Good overviw
Targets   https://www.globalgoals.org/11-sustainable-cities-and-communities
Where all the partners are listed https://sustainabledevelopment.un.org/partnerships/goal11/