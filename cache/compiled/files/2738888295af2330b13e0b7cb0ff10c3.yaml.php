<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/plugins/login-oauth2/login-oauth2.yaml',
    'modified' => 1621863992,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'button_style' => 'row',
        'require_grav_user' => false,
        'save_grav_user' => false,
        'store_provider_data' => true,
        'default_access_levels' => [
            'access' => [
                'site' => [
                    'login' => true
                ]
            ]
        ],
        'default_groups' => NULL,
        'providers' => [
            'github' => [
                'enabled' => true,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'user:email'
                    ]
                ]
            ],
            'instagram' => [
                'enabled' => true,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'basic',
                        1 => 'likes',
                        2 => 'comments'
                    ],
                    'host' => 'https://api.instagram.com'
                ]
            ],
            'facebook' => [
                'enabled' => true,
                'app_id' => '',
                'app_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'email',
                        1 => 'public_profile',
                        2 => 'user_hometown'
                    ],
                    'graph_api_version' => 'v2.10'
                ]
            ],
            'google' => [
                'enabled' => true,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'email',
                        1 => 'profile'
                    ],
                    'avatar_size' => 200,
                    'hd' => '*'
                ]
            ],
            'linkedin' => [
                'enabled' => true,
                'client_id' => '',
                'client_secret' => '',
                'options' => [
                    'scope' => [
                        0 => 'r_basicprofile',
                        1 => 'r_emailaddress'
                    ]
                ]
            ]
        ],
        'admin' => [
            'enabled' => false,
            'built_in_css' => true,
            'button_style' => 'row',
            'providers' => [
                'github' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'user',
                            1 => 'user:email'
                        ]
                    ]
                ],
                'instagram' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'basic',
                            1 => 'likes',
                            2 => 'comments'
                        ],
                        'host' => 'https://api.instagram.com'
                    ]
                ],
                'facebook' => [
                    'enabled' => false,
                    'app_id' => '',
                    'app_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'email',
                            1 => 'public_profile',
                            2 => 'user_hometown'
                        ],
                        'graph_api_version' => 'v2.10'
                    ]
                ],
                'google' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'email',
                            1 => 'profile'
                        ],
                        'avatar_size' => 200,
                        'hd' => '*'
                    ]
                ],
                'linkedin' => [
                    'enabled' => false,
                    'client_id' => '',
                    'client_secret' => '',
                    'options' => [
                        'scope' => [
                            0 => 'r_basicprofile',
                            1 => 'r_emailaddress'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
