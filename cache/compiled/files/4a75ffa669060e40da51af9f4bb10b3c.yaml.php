<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/accounts/tu02.yaml',
    'modified' => 1637039106,
    'data' => [
        'state' => 'enabled',
        'email' => 'youtube@husky.nz',
        'fullname' => 'Tu02',
        'level' => 'Newbie',
        'groups' => [
            0 => 'Users'
        ],
        'access' => [
            'site' => [
                'login' => 'true'
            ]
        ],
        'hashed_password' => '$2y$10$uPzvDk6Fk66vfbma/ryaVO8iRr2nrjydSEpgPDEpsZQtE.gZ4U8QC'
    ]
];
