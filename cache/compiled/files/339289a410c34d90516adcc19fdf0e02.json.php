<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledJsonFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/data/flex/indexes/pages.json',
    'modified' => 1637008648,
    'data' => [
        'version' => '1.5',
        'timestamp' => 1637008648,
        'count' => 4,
        'index' => [
            '' => [
                'key' => '',
                'storage_key' => '',
                'template' => NULL,
                'storage_timestamp' => 1636878697,
                'children' => [
                    '01.home' => 1636876101,
                    '02.typography' => 1636876101,
                    '03.sdg-gole-11' => 1636878697
                ],
                'checksum' => '80204b2392c9ac2c03dbb98a1de2a766'
            ],
            '01.home' => [
                'key' => 'home',
                'storage_key' => '01.home',
                'template' => 'default',
                'storage_timestamp' => 1636876101,
                'markdown' => [
                    '' => [
                        'default' => 1636876101
                    ]
                ],
                'checksum' => '0e2a2602fe87f65154eada4dfa70cdaa'
            ],
            '02.typography' => [
                'key' => 'typography',
                'storage_key' => '02.typography',
                'template' => 'default',
                'storage_timestamp' => 1636876101,
                'markdown' => [
                    '' => [
                        'default' => 1636876101
                    ]
                ],
                'checksum' => 'faf3319e28c6c9f35074ad963c2d0e33'
            ],
            '03.sdg-gole-11' => [
                'key' => 'sdg-gole-11',
                'storage_key' => '03.sdg-gole-11',
                'template' => 'default',
                'storage_timestamp' => 1637008647,
                'markdown' => [
                    '' => [
                        'default' => 1637008647
                    ]
                ],
                'checksum' => 'bbc60add8e440c005360fec44c48fe12'
            ]
        ]
    ]
];
