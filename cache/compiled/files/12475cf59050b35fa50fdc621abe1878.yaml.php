<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/config/groups.yaml',
    'modified' => 1637016704,
    'data' => [
        'Users' => [
            'access' => [
                'site' => [
                    'login' => true
                ],
                'admin' => [
                    'login' => false,
                    'super' => false,
                    'cache' => false,
                    'configuration' => false,
                    'pages' => false,
                    'maintenance' => false,
                    'statistics' => false,
                    'plugins' => false,
                    'themes' => false,
                    'tools' => false,
                    'users' => [
                        'create' => false,
                        'read' => false,
                        'update' => false,
                        'delete' => false,
                        'list' => true
                    ],
                    'contacts' => true,
                    'flex-objects' => false
                ]
            ],
            'readableName' => 'Users',
            'enabled' => true
        ],
        'Admins' => [
            'access' => [
                'site' => [
                    'login' => true
                ],
                'admin' => [
                    'login' => true,
                    'super' => true,
                    'cache' => true,
                    'configuration' => true,
                    'pages' => true,
                    'maintenance' => true,
                    'statistics' => true,
                    'plugins' => true,
                    'themes' => true,
                    'tools' => true,
                    'users' => true,
                    'contacts' => true,
                    'flex-objects' => true
                ]
            ],
            'readableName' => 'Admin',
            'enabled' => true
        ]
    ]
];
