<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/accounts/huskynzadmin.yaml',
    'modified' => 1637016878,
    'data' => [
        'state' => 'enabled',
        'email' => 'peter@husky.nz',
        'fullname' => 'Peter',
        'title' => 'Admin',
        'hashed_password' => '$2y$10$RjrlllSLVa6U3pRrE/vXOut5qBlMSqiqIkLUj9mIFSr0VhTL0TcZ.',
        'language' => 'en',
        'content_editor' => 'default',
        'twofa_enabled' => true,
        'twofa_secret' => 'TCIENJWXB3W7MTE74PSYVFGSC5Y2SW3J',
        'groups' => [
            0 => 'Admins'
        ],
        'avatar' => [
            
        ],
        'access' => [
            'site' => [
                'login' => true
            ],
            'admin' => [
                'login' => true,
                'super' => true
            ]
        ]
    ]
];
