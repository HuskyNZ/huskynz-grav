<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/config/plugins/email.yaml',
    'modified' => 1637037706,
    'data' => [
        'enabled' => true,
        'from' => 'Mail@husky.nz',
        'from_name' => NULL,
        'to' => 'peter@husky.nz',
        'to_name' => NULL,
        'queue' => [
            'enabled' => false,
            'flush_frequency' => '* * * * *',
            'flush_msg_limit' => 10,
            'flush_time_limit' => 100
        ],
        'mailer' => [
            'engine' => 'smtp',
            'smtp' => [
                'server' => 'in-v3.mailjet.com',
                'port' => 587,
                'encryption' => 'tls',
                'user' => '006aa949f2ea8c3db20f25b435fcea7d',
                'password' => 'e40f038b77aec066b9c75448de172453',
                'auth_mode' => NULL
            ],
            'sendmail' => [
                'bin' => '/usr/sbin/sendmail -bs'
            ]
        ],
        'content_type' => 'text/html',
        'debug' => false,
        'charset' => NULL,
        'cc' => NULL,
        'cc_name' => NULL,
        'bcc' => NULL,
        'reply_to' => NULL,
        'reply_to_name' => NULL,
        'body' => NULL
    ]
];
