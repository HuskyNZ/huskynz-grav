<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/config/versions.yaml',
    'modified' => 1637134378,
    'data' => [
        'core' => [
            'grav' => [
                'version' => '1.7.25',
                'schema' => '1.7.0_2020-11-20_1',
                'history' => [
                    0 => [
                        'version' => '1.7.25',
                        'date' => '2021-11-17 07:32:58'
                    ]
                ]
            ]
        ]
    ]
];
