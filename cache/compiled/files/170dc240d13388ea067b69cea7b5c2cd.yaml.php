<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/inetpub/wwwroot/grav/user/themes/aerial/aerial.yaml',
    'modified' => 1636877715,
    'data' => [
        'enabled' => true,
        'links' => [
            'twitter' => '#',
            'instagram' => '#',
            'facebook' => '#',
            'dribbble' => '#',
            'pinterest' => '#'
        ],
        'streams' => [
            'schemes' => [
                'theme' => [
                    'type' => 'ReadOnlyStream',
                    'paths' => [
                        0 => 'user/themes/aerial'
                    ]
                ]
            ]
        ]
    ]
];
